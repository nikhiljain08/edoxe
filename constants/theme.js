import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    primary: "#ffca28",
    secondary: "#707070",

    white: "#ffffff",
    black: "#000000",
    black_30: 'rgba(52, 52, 52, 0.3)',

    lightGray: "#707070", 
};

export const SIZES = {
    base: 8,
    font: 14,
    radius: 22,
    radiusSmall: 16,
    radiusMoreSmall: 8,
    padding: 10,
    padding2: 12,
    width,
    height,
    h1: 28,
    h2: 24,
    h3: 20,
    h4: 18,
    h5: 16,
    h6: 14,
};



export const FONTS = {
    h1: { fontFamily: "Roboto-Regular", fontSize: SIZES.h1, lineHeight: 36 },
    h2: { fontFamily: "Roboto-Regular", fontSize: SIZES.h2, lineHeight: 30 }
};

const appTheme = { COLORS, FONTS, SIZES };

export default appTheme;
export const user_ico = require("../assets/icons/user.png");
export const heart_ico = require("../assets/icons/heart.png");

export default {
    user_ico,
    heart_ico,
}
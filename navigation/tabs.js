import React from "react";
import { View, Image, TouchableOpacity } from "react-native";
import { createBottomTabNavigator, BottomTabBar } from "@react-navigation/bottom-tabs";
import { Home, MyAccount, MyCourse, MyWishlist } from "../screens";
import { COLORS, icons } from "../constants";
import Svg, { Path } from 'react-native-svg';
import { isIphoneX } from 'react-native-iphone-x-helper';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const Tabs = () => {
    return(
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
        
                    if (route.name === 'Home') {
                        iconName = focused ? 'home' : 'home-outline';
                    } else if (route.name === 'Course') {
                        iconName = focused ? 'albums' : 'albums-outline';
                    } else if (route.name === 'Wishlist') {
                        iconName = focused ? 'heart' : 'heart-outline';
                    } else if (route.name === 'Account') {
                        iconName = focused ? 'person' : 'person-outline';
                    }
                    return <Ionicons name={iconName} size={size} color={color} />;
                },
            })}
            tabBarOptions={{
                activeTintColor: COLORS.primary,
                inactiveTintColor: COLORS.secondary,
                style: { height: 60, padding:0, margin:0 },
                showLabel: false,
                showIcon: true,
            }}
        >
            <Tab.Screen name = "Home" component = {Home} options={{ tabBarBadge: 0 }} />
            <Tab.Screen name = "Course" component = {MyCourse} />
            <Tab.Screen name = "Wishlist" component = {MyWishlist} />
            <Tab.Screen name = "Account" component = {MyAccount} />
        </Tab.Navigator>
    );
}

export default Tabs;
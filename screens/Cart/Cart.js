import React, { useState, ReactNode } from "react";
import { Platform, StyleSheet, View, Text, TextInput, ScrollView, FlatList, ImageBackground, TouchableWithoutFeedback } from "react-native";
import Constants from 'expo-constants';
import { COLORS, SIZES, FONTS } from "../../constants/theme";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Cart = ({navigation}) => {
    let [fontsLoaded] = useFonts({
        'Roboto-Regular': require('../../assets/fonts/Roboto-Regular.ttf'),
    });
    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        return(
            <View style={styles.container}>
                <ScrollView style={styles.scrollView}>
                    <Ionicons style={[ styles.headerIcon ]} name="chevron-back-outline" size={24} color={"#fff"} onPress={() => navigation.goBack()}/>
                    <Text style={styles.title }>Cart</Text>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
    headerIcon: {
        backgroundColor: COLORS.black_30,
        borderRadius: SIZES.radiusSmall,
        padding: 12,
        alignSelf: 'flex-start',
        margin: 16
    }, 
});

export default Cart;
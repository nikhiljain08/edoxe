import React from "react";
import { Platform, StyleSheet, View, Text } from "react-native";
import Constants from 'expo-constants';

const Notifications = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Text>Notifications</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: 16,
      marginTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
});

export default Notifications;
import { StatusBar } from 'expo-status-bar';
import React, { useState } from "react";
import { Platform, StyleSheet, View, Text, Pressable, ScrollView } from "react-native";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import { COLORS, SIZES, FONTS } from "../../constants/theme";
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import Constants from 'expo-constants';

const MyAccount = ({navigation}) => {
    const [autheticated, setAutheticated] = useState(false);
    const user = auth().currentUser;
    
    auth().onAuthStateChanged((user) => {
        if(user) {
            setAutheticated(true);
        } else {
            setAutheticated(false);
        }
    });

    const signOut = async () => {
        try {
          await auth().signOut();
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
        } catch (error) {
          console.error(error);
        }
    };

    let [fontsLoaded] = useFonts({
        'Roboto-Regular': require('../../assets/fonts/Roboto-Regular.ttf'),
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        return(
            <View style={styles.container}>
            <StatusBar style='dark-content' />
                <ScrollView style={styles.scrollView} > 
                    <Text>My Account</Text>
                    {!autheticated && (<View style={styles.buttonContainer}>
                    <Pressable style={styles.signupbtn} onPress={() => navigation.replace("SignUp")}>
                        <Text style={styles.signuptxt}>Sign In</Text>
                    </Pressable>
                </View>)}
                </ScrollView>
                {autheticated && (<View style={styles.buttonContainer}>
                    <Pressable style={styles.signupbtn} onPress={() => signOut()}>
                        <Text style={styles.signuptxt}>Sign out</Text>
                    </Pressable>
                </View>)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
    buttonContainer: {
        textAlign: 'center',
    },
    signupbtn: {
        backgroundColor: COLORS.primary,
        padding: 16,
        borderRadius: SIZES.radius,
        alignItems: 'center',
        margin: 0
    },
    signuptxt: {
        color: COLORS.white,
        fontSize: SIZES.h4,
        fontFamily: "Roboto-Regular"
    },
});

export default MyAccount;
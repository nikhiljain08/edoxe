import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, TextInput, ScrollView, Button, ImageBackground, TouchableWithoutFeedback, Pressable, Platform } from "react-native";
import Constants from 'expo-constants';
import { COLORS, SIZES, FONTS } from "../../../constants/theme";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import Ionicons from 'react-native-vector-icons/Ionicons';
import auth from '@react-native-firebase/auth';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import { ErrorMessage } from '../../../components';

const SignUp = ({navigation}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isShowPassowrd, setShowPassword] = useState(false);
    const [signupError, setSignupError] = useState('');

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['email','profile'],
            webClientId:'379705880752-k0moc737is8f24e3eoc29i2copn1tb9o.apps.googleusercontent.com',
            offlineAccess: true,
        });
    }, []);

    let [fontsLoaded] = useFonts({
        'Roboto-Regular': require('../../../assets/fonts/Roboto-Regular.ttf'),
    });
    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        
        const signUp = async () => {
            try {
                if(validate()) {
                    setSignupError('');
                    await auth.createUserWithEmailAndPassword(email, password);
                } else {
                    setSignupError("Invalid Email or password!");
                }
              } catch (error) {
                setSignupError(error.message);
              }
        }

        const validate = () => {
            const strongRegex = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");

            if (!strongRegex.test(email)) {
                alert("Email is invalid!")
                return false;
            } else if (password.length < 8) {
                alert("Password is invalid!");
                return false;
            }
            return true;
        }

        const _signIn = async () => {
            try {
                await GoogleSignin.hasPlayServices();
                const { idToken } = await GoogleSignin.signIn();
                const googleCredential = auth.GoogleAuthProvider.credential(idToken);
                return await auth().signInWithCredential(googleCredential);
            } catch (error) {
              if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                    console.log('Cancel');
              } else if (error.code === statusCodes.IN_PROGRESS) {
                    console.log('Signin in progress');
              } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                    alert('PLAY_SERVICES_NOT_AVAILABLE');
              } else {
                    console.log("error -> " + error);
              }
            }
        };

        return(
            <View style={styles.container}>
                <StatusBar style='dark-content' />
                <Text style={styles.title }>Create account</Text>
                <Text style={styles.subtitle }>Create your account to start your course lessons.</Text>

                <View style={styles.signUpContainer}>
                    <View style={styles.sectionGoogleContainer}>
                        <GoogleSigninButton
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                            onPress={() => _signIn().then(() => navigation.replace("Home"))}
                        />
                    </View>

                    <Text style={styles.signupor }>Or sign up with your email</Text>

                    <View style={styles.inputSectionContainer}>
                        <View style={styles.inputSection} >
                            <Ionicons style={styles.inputIcon} name="mail-outline" size={20} color={"#000"}/>
                            <TextInput 
                                style={styles.input} 
                                placeholder="Your email" 
                                onChangeText={(text) => setEmail(text)}
                                textContentType='emailAddress'
                                keyboardType='email-address'
                                autoCapitalize='none'
                                autoCorrect={false}
                                returnKeyType="next" 
                                autoCompleteType='email'/>
                        </View>
                        <View style={styles.inputSection} >
                            <Ionicons style={styles.inputIcon} name="lock-closed-outline" size={20} color={"#000"}/>
                            <TextInput 
                                style={styles.input} 
                                placeholder="Your password" 
                                secureTextEntry={!isShowPassowrd} 
                                onChangeText={(text) => setPassword(text)}/>
                            <Ionicons style={styles.inputIcon} name={ isShowPassowrd ? "eye-outline" : "eye-off-outline"} size={20} color={"#000"} onPress={() => setShowPassword(!isShowPassowrd)}/>
                        </View>
                    </View>
                    {signupError ? <ErrorMessage error={signupError} visible={true} /> : null}
                    <Pressable style={styles.signupbtn} onPress={() => signUp()}>
                        <Text style={styles.signuptxt}>Sign Up</Text>
                    </Pressable>
                    <Text style={styles.tnc }>By continuing, you agree to our Terms & Conditions and Privacy Policy</Text>
                </View>
                <View style={ styles.bottomtitleContainer }>
                    <Pressable  onPress={() => navigation.navigate("Login")}>
                        <Text style={ styles.bottomtitle }>I already have an account</Text>
                    </Pressable>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
    title: {
        color: COLORS.black,
        marginTop: 32,
        marginLeft: 16,
        fontSize: SIZES.h1,
        fontFamily: "Roboto-Regular"
    },
    subtitle: {
        marginTop: 4,
        marginLeft: 16,
        color: COLORS.lightGray,
        fontFamily: "Roboto-Regular"
    },
    bottomtitleContainer: {
        alignItems: "center",
        backgroundColor: COLORS.primary
    },
    bottomtitle: {
        margin: 0,
        color: COLORS.primary,
        fontFamily: "Roboto-Regular",
        textAlign: 'center', 
        bottom: 32,
        position: "absolute",
    },
    headerIcon: {
        backgroundColor: COLORS.black_30,
        borderRadius: SIZES.radiusSmall,
        padding: 12,
        alignSelf: 'flex-start',
        margin: 16
    },
    signUpContainer: {
        alignSelf: "center",
        flex: 1
    },
    inputSectionContainer: {
        marginTop: 8
    },
    inputSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 55,
        borderRadius: SIZES.radius,
        marginTop: 16,
        marginLeft: 16,
        marginRight: 16
    },
    inputIcon: {
        padding: 18,
    },
    input: {
        flex: 1,
        backgroundColor: COLORS.white,
        borderRadius: SIZES.radius,
        fontFamily: "Roboto-Regular"
    },
    signupbtn: {
        backgroundColor: COLORS.primary,
        padding: 16,
        borderRadius: SIZES.radius,
        alignItems: 'center',
        margin: 16
    },
    signuptxt: {
        color: COLORS.white,
        fontSize: SIZES.h4,
        fontFamily: "Roboto-Regular"
    },
    signupor: {
        margin: 16,
        color: COLORS.lightGray,
        fontFamily: "Roboto-Regular",
        textAlign: 'center',
        marginTop: 32,
    },
    tnc: {
        margin: 16,
        color: COLORS.lightGray,
        fontFamily: "Roboto-Regular",
        textAlign: 'center', 
    },
    sectionGoogleContainer: {
        alignItems: "center",
        marginTop: 48,
    }
});

export default SignUp;
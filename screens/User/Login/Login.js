import React, { useState } from "react";
import { Platform, StyleSheet, View, Text, TextInput, ScrollView, FlatList, ImageBackground, TouchableWithoutFeedback, Modal, Pressable, Alert } from "react-native";
import { COLORS, SIZES, FONTS } from "../../../constants/theme";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import Ionicons from 'react-native-vector-icons/Ionicons';
import auth from '@react-native-firebase/auth';

const Login = () => {
    let [fontsLoaded] = useFonts({
        'Roboto-Regular': require('../../../assets/fonts/Roboto-Regular.ttf'),
    });

    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        return(
            <View style={styles.container}>
                <Text>Login</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
});

export default Login;
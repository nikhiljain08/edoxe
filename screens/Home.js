import { StatusBar } from 'expo-status-bar';
import React, { useState } from "react";
import { Platform, StyleSheet, View, Text, TextInput, ScrollView, FlatList, ImageBackground, TouchableWithoutFeedback, Modal, Pressable, Alert } from "react-native";
import { COLORS, SIZES, FONTS } from "../constants/theme";
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';
import Ionicons from 'react-native-vector-icons/Ionicons';
import auth from '@react-native-firebase/auth';
import Constants from 'expo-constants';

var username = "User";

const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'First Item',
      author: 'Nikhil Jain',
      img: 'https://reactnative.dev/img/tiny_logo.png',
      price: 40,
      tags: 'best seller',
      fav: true,
      rating: 5
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'Second Item',
      author: 'Nikhil Jain',
      img: 'https://reactnative.dev/img/tiny_logo.png',
      price: 40,
      tags: 'best seller',
      fav: true,
      rating: 5
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'Third Item',
      author: 'Nikhil Jain',
      img: 'https://reactnative.dev/img/tiny_logo.png',
      price: 40,
      tags: 'best seller',
      fav: true,
      rating: 5
    },
    {
      id: '58694a0f-3da1-471f-bd96-123571e29d72',
      title: 'Third Item',
      author: 'Nikhil Jain',
      img: 'https://reactnative.dev/img/tiny_logo.png',
      price: 40,
      tags: 'best seller',
      fav: true,
      rating: 5
    },
    {
      id: '58694a0f-3da1-471f-bd96-145572329d72',
      title: 'Third Item',
      author: 'Nikhil Jain',
      img: 'https://reactnative.dev/img/tiny_logo.png',
      price: 40,
      tags: 'best seller',
      fav: true,
      rating: 5
    },
    {
      id: '58694a0f-3da1-471f-bd96-1455651e29d72',
      title: 'Third Item',
      author: 'Nikhil Jain',
      img: 'https://reactnative.dev/img/tiny_logo.png',
      price: 40,
      tags: 'best seller',
      fav: true,
      rating: 5
    },

  ];


const Item = ({ item }) => (
    <View style={styles.listItem}>
        <View>
            <ImageBackground source={{ uri: item.img}} style={styles.stretch} resizeMode= 'stretch'>
                <Text style={styles.listItemRating}>&#x2B50; {item.rating}</Text>
            </ImageBackground>
        </View>
        <Text style={styles.listItemTitle}>{item.title}</Text>
        <Text style={styles.listItemSubTitle}>{item.author}</Text>
        <View style={styles.listItemPriceRow}> 
            <Text style={styles.listItemPrice}>₹{item.price}</Text>
            <Text style={styles.listItemTags}>{item.tags}</Text>
        </View>
        <Text>{item.fav}</Text>
    </View>
);

const Home = ({navigation}) => {
    const [selectedId, setSelectedId] = useState(null);
    const [cartModalVisible, setCartModalVisible] = useState(false);
    const [notiModalVisible, setNotiModalVisible] = useState(false);
    const user = auth().currentUser;

    const renderItem = ({ item }) => (
        <Item item={item} onPress={() => setSelectedId(item.id)} />
    );

    let [fontsLoaded] = useFonts({
        'Roboto-Regular': require('../assets/fonts/Roboto-Regular.ttf'),
    });
    if (!fontsLoaded) {
        return <AppLoading />;
    } else {
        
        if(user) {
            username = user.displayName.split(" ")[0];
        } else {
            username = "User";
        }
        return(
            <View style={styles.container}>
                <StatusBar style='dark-content' />
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={cartModalVisible}
                    onRequestClose={() => {
                        setCartModalVisible(!cartModalVisible);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Ionicons style={[ styles.modalIcon ]} name="close-outline" size={24} color={"#fff"} onPress={() => setCartModalVisible(!cartModalVisible)}/>
                            <Text style={styles.modalText}>Cart!</Text>
                        </View>
                    </View>
                </Modal>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={notiModalVisible}
                    onRequestClose={() => {
                        setNotiModalVisible(!notiModalVisible);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Ionicons style={[ styles.modalIcon ]} name="close-outline" size={24} color={"#fff"} onPress={() => setNotiModalVisible(!notiModalVisible)}/>
                            <View style={styles.headerRowText}>
                                <Text style={ styles.modalText }>Notifications &#128075;</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
                <ScrollView style={styles.scrollView}>
                    <View style={styles.header}>
                        <View style={styles.headerRow}>
                            <View style={styles.headerRowIcons}>
                                <Ionicons style={[ styles.headerIcon, styles.headerIconCart ]} name="cart" size={24} color={"#fff"} onPress={() => setCartModalVisible(true)}/>
                                <Ionicons style={ styles.headerIcon} name="notifications" size={24} color={"#fff"} onPress={() => setNotiModalVisible(true)}/>
                            </View>                        
                            <View style={styles.headerRowText}>
                                <Text style={ styles.title }>Hi, { username } &#128075;</Text>
                                <Text style={ styles.subtitle }>Let's start learning!</Text>
                            </View>
                        </View>
                        <TouchableWithoutFeedback onPress={() => navigation.navigate('Search')}>
                            <View style={styles.searchSection} >
                                <Ionicons style={styles.searchIcon} name="search-outline" size={24} color={"#000"}/>
                                <TextInput style={styles.input} placeholder="Search for anything" editable={false} />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <View>
                        <View style={styles.listHeader}>
                            <Text style={styles.listtitle}>Popular Courses</Text>
                            <Text style={styles.listSeeAll}>See all</Text>
                        </View>
                        <FlatList
                            horizontal
                            data={DATA}
                            renderItem={renderItem}
                            keyExtractor={item => item.id}
                            extraData={selectedId}
                        />
                    </View>
                    <View>
                        <View style={styles.listHeader}>
                            <Text style={styles.listtitle}>Categories</Text>
                            <Text style={styles.listSeeAll}>See all</Text>
                        </View>
                        <FlatList
                            horizontal
                            data={DATA}
                            renderItem={renderItem}
                            keyExtractor={item => item.id}
                            extraData={selectedId}
                        />
                    </View>
                    <View>
                        <View style={styles.listHeader}>
                            <Text style={styles.listtitle}>Favourites</Text>
                            <Text style={styles.listSeeAll}>See all</Text>
                        </View>
                        <FlatList
                            horizontal
                            data={DATA}
                            renderItem={renderItem}
                            keyExtractor={item => item.id}
                            extraData={selectedId}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
    header: {
        backgroundColor: COLORS.primary,
        borderBottomEndRadius: SIZES.radius,
        borderBottomRightRadius: SIZES.radius,
        borderBottomStartRadius: SIZES.radius,
        borderBottomLeftRadius: SIZES.radius,
    },
    headerRow: {
        marginTop: 32,
        marginLeft: 24,
        marginBottom: 24,
        marginRight: 24,
        flexDirection: 'row-reverse',
    },
    headerRowText: {
        flex: 1,
        marginTop: 0
    },
    headerRowIcons: {
        flexDirection: 'row',
        alignSelf: 'center'
    },
    headerIcon: {
        backgroundColor: COLORS.black_30,
        borderRadius: SIZES.radiusSmall,
        padding: 12,
        justifyContent: "center"
    }, 
    headerIconCart: {
        marginRight: 10
    },
    title: {
        color: COLORS.white,
        fontSize: SIZES.h1,
        fontFamily: "Roboto-Regular"
    },
    subtitle: {
        marginTop: 4,
        color: COLORS.white,
        fontFamily: "Roboto-Regular"
    },
    listHeader: {
        flexDirection: 'row',
        marginTop: 16,
        marginLeft: 16,
        marginRight: 16
    },
    listSeeAll: {
        fontSize: SIZES.h5,
        color: COLORS.primary,
        fontFamily: "Roboto-Regular",
        alignSelf: 'flex-end'
    },
    listtitle: {
        flex: 1,
        fontSize: SIZES.h4,
        color: COLORS.black,
        fontFamily: "Roboto-Regular"
    },
    searchSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 55,
        borderRadius: SIZES.radius,
        marginTop: 48,
        margin: 24
    },
    searchIcon: {
        padding: 14,
    },
    input: {
        flex: 1,
        backgroundColor: COLORS.white,
        borderRadius: SIZES.radius,
        fontFamily: "Roboto-Regular"
    },
    listItem: {
        margin: 12
    },
    listItemPriceRow: {
        flexDirection: 'row',
        marginTop: 4
    },
    listItemTitle: {
        color: COLORS.black,
        fontSize: SIZES.h4,
        fontFamily: "Roboto-Regular"
    },
    listItemSubTitle: {
        color: COLORS.lightGray,
        fontSize: SIZES.h6,
        fontFamily: "Roboto-Regular"
    },
    listItemPrice: {
        flex: 1, 
        color: COLORS.primary,
        fontSize: SIZES.h3,
        fontFamily: "Roboto-Regular"
    },
    listItemTags: {
        color: COLORS.white,
        fontSize: SIZES.h6,
        fontFamily: "Roboto-Regular",
        backgroundColor: COLORS.primary,
        padding: 2,
        paddingLeft: 8, 
        paddingRight: 8, 
        borderRadius: SIZES.radiusMoreSmall,
        alignSelf: 'flex-end'
    },
    listItemRating: {
        color: COLORS.black,
        fontSize: SIZES.h6,
        fontFamily: "Roboto-Regular",
        backgroundColor: COLORS.white,
        padding: 2,
        paddingLeft: 8, 
        paddingRight: 8, 
        borderRadius: SIZES.radiusMoreSmall,
        alignSelf: 'flex-start'
    },
    stretch: {
        width: 150,
        height: 100,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    modalView: {
        margin: 0,
        backgroundColor: "white",
        borderTopStartRadius: SIZES.radius,
        borderTopEndRadius: SIZES.radius,
        padding: 16,
        bottom: 0,
        height: '85%',
        width: '100%',
        position: 'absolute', 
    },
    modalIcon: {
        backgroundColor: COLORS.black_30,
        borderRadius: SIZES.radiusSmall,
        padding: 12,
        alignSelf: 'flex-end',
        margin: 4
    }, 
    modalText: {
        color: COLORS.black,
        fontSize: SIZES.h2,
        fontFamily: "Roboto-Regular"
    }
});

export default Home;
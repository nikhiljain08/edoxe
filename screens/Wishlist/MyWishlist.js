import { StatusBar } from 'expo-status-bar';
import React from "react";
import { Platform, StyleSheet, View, Text } from "react-native";
import Constants from 'expo-constants';

const MyWishlist = () => {
    return(
        <View style={styles.container}>
            <StatusBar style='dark-content' />
            <Text>My Wishlist</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: 16,
      marginTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight + 16
    },
});

export default MyWishlist;
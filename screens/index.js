import Home from "./Home";
import MyCourse from "./My-Course/MyCourse";
import MyAccount from "./Account/MyAccount";
import MyWishlist from "./Wishlist/MyWishlist";
import Notifications from "./Notifications/Notifications";
import Cart from "./Cart/Cart";
import Search from "./Search/Search";
import SignUp from "./User/SignUp/SignUp";
import Login from "./User/Login/Login";

export {
    Home,
    MyAccount,
    MyCourse,
    MyWishlist,
    Notifications,
    Cart,
    Search,
    SignUp,
    Login,
}
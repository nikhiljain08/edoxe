import React from "react";
import { Platform, StyleSheet, View, Text } from "react-native";
import Constants from 'expo-constants';

const Search = ({navigation}) => {
    return(
        <View style={styles.container}>
            <Text>Search</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      margin: 16,
      marginTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
});

export default Search;
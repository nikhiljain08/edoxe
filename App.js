import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { MyCourse, MyWishlist, MyAccount, Cart, Notifications, Search, SignUp, Login } from "./screens";
import Tabs from "./navigation/tabs";

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator
            headerMode='none'
            initialRouteName= {"SignUp"}>
            <Stack.Screen name="Home" component={ Tabs } />
            <Stack.Screen name="MyCourse" component={ MyCourse } />
            <Stack.Screen name="MyWishlist" component={ MyWishlist } />
            <Stack.Screen name="MyAccount" component={ MyAccount } />
            <Stack.Screen name="Cart" component={ Cart } />
            <Stack.Screen name="Notifications" component={ Notifications } />
            <Stack.Screen name="Search" component={ Search } />
            <Stack.Screen name="SignUp" component={ SignUp } />
            <Stack.Screen name="Login" component={ Login } />
        </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
